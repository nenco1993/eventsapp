package com.example.neven.thedistanceuk.views;

/**
 * Created by Neven on 11.7.2017..
 */
public interface BaseView {

    void showErrorMessage(String message);
}
