package com.example.neven.thedistanceuk.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.*;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.neven.thedistanceuk.MyApplication;
import com.example.neven.thedistanceuk.R;
import com.example.neven.thedistanceuk.adapters.EventsAdapter;
import com.example.neven.thedistanceuk.dagger.modules.EventsModule;
import com.example.neven.thedistanceuk.models.Event;
import com.example.neven.thedistanceuk.presenters.EventsPresenter;
import com.example.neven.thedistanceuk.views.EventsView;

import javax.inject.Inject;
import java.util.List;

public class MainActivity extends BaseActivity implements EventsView, EventsAdapter.EventClickListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.pbMain)
    ProgressBar progressBar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    EventsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ((MyApplication) getApplication()).appComponent.newEventsSubComponent(new EventsModule(this)).inject(this);
        presenter.loadData();
        System.out.println("eeeeee");

        LinearLayoutManager manager = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


    }

    @Override
    public void showData(List<Event> listEvents) {

        EventsAdapter adapter = new EventsAdapter(listEvents, getBaseContext());
        recyclerView.setAdapter(adapter);
        progressBar.setVisibility(View.INVISIBLE);
        adapter.setEventClickListener(this);


    }

    @Override
    public void onEventClicked(Event event) {

        Intent intent = new Intent(getBaseContext(), EventDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("event", event);
        intent.putExtras(bundle);
        startActivity(intent);


    }

    @Override
    public void showErrorMessage(String message) {

        // option 1: BroadcastReceiver that always listens for this action.
        // option 2; go to some screen that says that you dont have an internet connection and has a retry button

        // In this case I'm just gonna print the message

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();


    }
}
