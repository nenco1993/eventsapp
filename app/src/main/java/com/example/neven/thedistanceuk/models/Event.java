
package com.example.neven.thedistanceuk.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Event implements Serializable {

    @SerializedName("name")
    @Expose
    public Name name;
    @SerializedName("description")
    @Expose
    public Description description;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("vanity_url")
    @Expose
    public String vanityUrl;
    @SerializedName("start")
    @Expose
    public Start start;
    @SerializedName("end")
    @Expose
    public End end;
    @SerializedName("created")
    @Expose
    public String created;
    @SerializedName("changed")
    @Expose
    public String changed;
    @SerializedName("capacity")
    @Expose
    public Integer capacity;
    @SerializedName("capacity_is_custom")
    @Expose
    public Boolean capacityIsCustom;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("currency")
    @Expose
    public String currency;
    @SerializedName("listed")
    @Expose
    public Boolean listed;
    @SerializedName("shareable")
    @Expose
    public Boolean shareable;
    @SerializedName("online_event")
    @Expose
    public Boolean onlineEvent;
    @SerializedName("tx_time_limit")
    @Expose
    public Integer txTimeLimit;
    @SerializedName("hide_start_date")
    @Expose
    public Boolean hideStartDate;
    @SerializedName("hide_end_date")
    @Expose
    public Boolean hideEndDate;
    @SerializedName("locale")
    @Expose
    public String locale;
    @SerializedName("is_locked")
    @Expose
    public Boolean isLocked;
    @SerializedName("privacy_setting")
    @Expose
    public String privacySetting;
    @SerializedName("is_series")
    @Expose
    public Boolean isSeries;
    @SerializedName("is_series_parent")
    @Expose
    public Boolean isSeriesParent;
    @SerializedName("is_reserved_seating")
    @Expose
    public Boolean isReservedSeating;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("is_free")
    @Expose
    public Boolean isFree;
    @SerializedName("version")
    @Expose
    public String version;
    @SerializedName("logo_id")
    @Expose
    public String logoId;
    @SerializedName("organizer_id")
    @Expose
    public String organizerId;
    @SerializedName("venue_id")
    @Expose
    public String venueId;
    @SerializedName("category_id")
    @Expose
    public String categoryId;
    
    @SerializedName("format_id")
    @Expose
    public String formatId;
    @SerializedName("resource_uri")
    @Expose
    public String resourceUri;
    @SerializedName("logo")
    @Expose
    public Logo logo;

 

}
