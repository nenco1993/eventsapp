
package com.example.neven.thedistanceuk.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Events implements Serializable {

    @SerializedName("pagination")
    @Expose
    public Pagination pagination;
    @SerializedName("events")
    @Expose
    public final List<Event> events = null;

  

}
