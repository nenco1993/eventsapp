package com.example.neven.thedistanceuk.dagger.components;

import com.example.neven.thedistanceuk.activities.MainActivity;
import com.example.neven.thedistanceuk.dagger.modules.EventsModule;
import com.example.neven.thedistanceuk.dagger.scopes.ActivityScope;
import dagger.Subcomponent;

/**
 * Created by Neven on 11.7.2017..
 */
@ActivityScope
@Subcomponent(modules = EventsModule.class)
public interface EventsComponent {

    void inject(MainActivity activity);

}
