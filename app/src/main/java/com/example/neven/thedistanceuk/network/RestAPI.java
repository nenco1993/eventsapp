package com.example.neven.thedistanceuk.network;

import com.example.neven.thedistanceuk.models.Events;
import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Neven on 11.7.2017..
 */
public interface RestAPI {

    @GET("events/search/?token=VBUSKKCQ2VTXKPOP34PX")
    Observable<Events> getEvents();

}
