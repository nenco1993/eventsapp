package com.example.neven.thedistanceuk.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.neven.thedistanceuk.R;
import com.example.neven.thedistanceuk.models.Event;
import com.example.neven.thedistanceuk.utils.MyWebView;

public class EventDetailsActivity extends AppCompatActivity {

    @BindView(R.id.webview)
    WebView webView;

    @BindView(R.id.bMoreInfo)
    Button bMoreInfo;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);


        if (getIntent() != null) {
            Event event = (Event) getIntent().getSerializableExtra("event");
            webView.setWebViewClient(new MyWebView(progressBar));
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadData(event.description.html, "text/html", "UTF-8");
            webView.setVisibility(View.VISIBLE);

            bMoreInfo.setOnClickListener(view -> {
                webView.setVisibility(View.GONE);
                bMoreInfo.setVisibility(View.GONE);
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) webView.getLayoutParams();
                params.bottomMargin = 0;
                webView.setLayoutParams(params);
                webView.loadUrl(event.url);


            });


        }


    }

}
