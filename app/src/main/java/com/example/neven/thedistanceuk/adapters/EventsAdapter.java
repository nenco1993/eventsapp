package com.example.neven.thedistanceuk.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.example.neven.thedistanceuk.R;
import com.example.neven.thedistanceuk.models.Event;

import java.util.List;

/**
 * Created by Neven on 11.7.2017..
 */
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.MyViewHolder> {

    private final List<Event> listEvent;
    private final Context context;
    private EventClickListener eventClickListener;

    public EventsAdapter(List<Event> listEvent, Context context) {
        this.listEvent = listEvent;
        this.context = context;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvEventName)
        TextView tvEventName;

        @BindView(R.id.tvEventDate)
        TextView tvEventDate;

        @BindView(R.id.ivLogo)
        ImageView ivLogo;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Event singleEvent = listEvent.get(position);

        Glide
                .with(context)
                .load(singleEvent.logo.url)
                .placeholder(R.drawable.placeholder_events)
                .crossFade()
                .into(holder.ivLogo);

        holder.tvEventName.setText(singleEvent.name.text);
        holder.tvEventDate.setText(formatDate(singleEvent.start.local));

        holder.itemView.setOnClickListener(view -> {

            if (eventClickListener != null) {

                eventClickListener.onEventClicked(singleEvent);
            }


        });

    }

    public void setEventClickListener(EventClickListener eventClickListener) {
        this.eventClickListener = eventClickListener;
    }

    @Override
    public int getItemCount() {
        return listEvent.size();
    }

    public interface EventClickListener {

        void onEventClicked(Event event);
    }

    private String formatDate(String date) {

        String formatedDate;
        String[] separated = date.split("T");
        formatedDate = separated[0] + " AT " + separated[1];

        return formatedDate;


    }


}
