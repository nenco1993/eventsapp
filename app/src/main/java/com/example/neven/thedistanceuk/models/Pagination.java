
package com.example.neven.thedistanceuk.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Pagination implements Serializable{

    @SerializedName("object_count")
    @Expose
    public Integer objectCount;
    @SerializedName("page_number")
    @Expose
    public Integer pageNumber;
    @SerializedName("page_size")
    @Expose
    public Integer pageSize;
    @SerializedName("page_count")
    @Expose
    public Integer pageCount;
    @SerializedName("has_more_items")
    @Expose
    public Boolean hasMoreItems;

   

}
