package com.example.neven.thedistanceuk.interactors;

import com.example.neven.thedistanceuk.listeners.EventsListener;
import com.example.neven.thedistanceuk.models.Events;
import com.example.neven.thedistanceuk.network.RestAPI;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import javax.inject.Inject;

/**
 * Created by Neven on 11.7.2017..
 */
public class EventsInteractorImpl implements EventsInteractor {

    private final Retrofit retrofit;

    @Inject
    public EventsInteractorImpl(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public void downloadData(EventsListener listener) {

        RestAPI api = retrofit.create(RestAPI.class);

        Observable<Events> observable = api.getEvents();

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        listener::onSuccess,
                        listener::onFailure
                );


    }
}
