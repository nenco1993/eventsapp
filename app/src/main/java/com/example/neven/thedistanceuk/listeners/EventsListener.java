package com.example.neven.thedistanceuk.listeners;

import com.example.neven.thedistanceuk.models.Events;

/**
 * Created by Neven on 11.7.2017..
 */
public interface EventsListener extends BaseListener {

    void onSuccess(Events events);


}
