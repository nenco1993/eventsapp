package com.example.neven.thedistanceuk.views;

import com.example.neven.thedistanceuk.models.Event;

import java.util.List;

/**
 * Created by Neven on 11.7.2017..
 */
public interface EventsView extends BaseView {

    void showData(List<Event>listEvents);


}
