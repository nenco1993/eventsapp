package com.example.neven.thedistanceuk;

import android.app.Application;
import com.example.neven.thedistanceuk.dagger.components.AppComponent;
import com.example.neven.thedistanceuk.dagger.components.DaggerAppComponent;

/**
 * Created by Neven on 11.7.2017..
 */
public class MyApplication extends Application {

    public AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.create();

    }
}
