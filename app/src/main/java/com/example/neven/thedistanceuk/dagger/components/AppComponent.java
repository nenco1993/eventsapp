package com.example.neven.thedistanceuk.dagger.components;

import com.example.neven.thedistanceuk.activities.BaseActivity;
import com.example.neven.thedistanceuk.dagger.modules.EventsModule;
import com.example.neven.thedistanceuk.dagger.modules.NetModule;
import dagger.Component;

import javax.inject.Singleton;

/**
 * Created by Neven on 11.7.2017..
 */
@Singleton
@Component(modules = NetModule.class)
public interface AppComponent {

    void inject(BaseActivity activity);
    EventsComponent newEventsSubComponent(EventsModule module);


}
