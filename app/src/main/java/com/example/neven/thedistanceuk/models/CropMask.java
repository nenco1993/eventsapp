
package com.example.neven.thedistanceuk.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CropMask implements Serializable {

    @SerializedName("top_left")
    @Expose
    public TopLeft topLeft;
    @SerializedName("width")
    @Expose
    public Integer width;
    @SerializedName("height")
    @Expose
    public Integer height;

   

}
