package com.example.neven.thedistanceuk.interactors;

import com.example.neven.thedistanceuk.listeners.EventsListener;

/**
 * Created by Neven on 11.7.2017..
 */
public interface EventsInteractor {

    void downloadData(EventsListener listener);

}
