
package com.example.neven.thedistanceuk.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Logo implements Serializable {

    @SerializedName("crop_mask")
    @Expose
    public CropMask cropMask;
    @SerializedName("original")
    @Expose
    public Original original;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("aspect_ratio")
    @Expose
    public String aspectRatio;
    @SerializedName("edge_color")
    @Expose
    public String edgeColor;
    @SerializedName("edge_color_set")
    @Expose
    public Boolean edgeColorSet;
    
    

   

}
