package com.example.neven.thedistanceuk.presenters;

import com.example.neven.thedistanceuk.interactors.EventsInteractor;
import com.example.neven.thedistanceuk.listeners.EventsListener;
import com.example.neven.thedistanceuk.models.Events;
import com.example.neven.thedistanceuk.views.EventsView;
import retrofit2.HttpException;

import javax.inject.Inject;
import java.io.IOException;

/**
 * Created by Neven on 11.7.2017..
 */
public class EventsPresenterImpl implements EventsPresenter, EventsListener {

    private final EventsInteractor interactor;
    private final EventsView view;

    @Inject
    public EventsPresenterImpl(EventsInteractor interactor, EventsView view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void loadData() {

        interactor.downloadData(this);

    }

    @Override
    public void onFailure(Throwable throwable) {

        if (throwable instanceof IOException) {
            throwable.printStackTrace();
            view.showErrorMessage("Check your internet connection");
        } else if (throwable instanceof HttpException) {
            throwable.printStackTrace();
            view.showErrorMessage("We will be back soon!");
        } else {
            throwable.printStackTrace();
        }


    }

    @Override
    public void onSuccess(Events events) {

        view.showData(events.events);


    }
}
