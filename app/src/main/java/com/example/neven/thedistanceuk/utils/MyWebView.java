package com.example.neven.thedistanceuk.utils;

import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * Created by Neven on 11.7.2017..
 */
public class MyWebView extends WebViewClient {

    private final ProgressBar progressBar;

    public MyWebView(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        progressBar.setVisibility(View.INVISIBLE);
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        progressBar.setVisibility(View.VISIBLE);
        view.setVisibility(View.GONE);
    }
}
