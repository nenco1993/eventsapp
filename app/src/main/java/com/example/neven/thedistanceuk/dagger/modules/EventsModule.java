package com.example.neven.thedistanceuk.dagger.modules;

import com.example.neven.thedistanceuk.dagger.scopes.ActivityScope;
import com.example.neven.thedistanceuk.interactors.EventsInteractor;
import com.example.neven.thedistanceuk.interactors.EventsInteractorImpl;
import com.example.neven.thedistanceuk.presenters.EventsPresenter;
import com.example.neven.thedistanceuk.presenters.EventsPresenterImpl;
import com.example.neven.thedistanceuk.views.EventsView;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Neven on 11.7.2017..
 */
@Module
public class EventsModule {

    private final EventsView view;

    public EventsModule(EventsView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    EventsPresenter provideEventsPresenter(EventsPresenterImpl presenter){

        return presenter;
    }

    @Provides
    @ActivityScope
    EventsInteractor provideEventsInteractor(EventsInteractorImpl interactor){

        return interactor;
    }

    @Provides
    @ActivityScope
    EventsView provideView(){

        return view;
    }





}
