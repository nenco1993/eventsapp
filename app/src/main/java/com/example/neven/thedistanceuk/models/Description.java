
package com.example.neven.thedistanceuk.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Description implements Serializable {

    @SerializedName("text")
    @Expose
    public String text;
    @SerializedName("html")
    @Expose
    public String html;

  

}
