package com.example.neven.thedistanceuk.listeners;

/**
 * Created by Neven on 11.7.2017..
 */
public interface BaseListener {

    void onFailure(Throwable throwable);

}
